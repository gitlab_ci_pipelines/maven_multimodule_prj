FROM openjdk:8-jdk-alpine
COPY module1/target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/app.jar"]
